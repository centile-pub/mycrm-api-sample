# mycrm-api-sample

This is an example showing the usage of the myCrm API:
* a login as an ENDUSER (with ACD Supervisor rights if needs be)
* Getting some information on the ISTRA release and the user
* Getting the list of user's terminals
* Establish a Web Socket connection
* Putting 2 listeners (CallLines and ACDSupervision)
* Getting ACD Groups list
* Monitoring regular calls and ACD calls
* this script was written to help developer understand the use of myCrm API

# Pre-requisites
* Use python3 (3.8 recommended)
* Import useful modules 
# Quick start

Clone the repository:

    git clone git@gitlab.com:centile-pub/myrcc-api-sample

# Launch the script

    python3.8 myCrm_websocket.py

# More info

Please refer to our PartnerConnect website (https://partnerconnect.centile.com) or contact me.
