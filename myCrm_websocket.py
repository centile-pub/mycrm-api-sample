# Importing some useful libraries

import requests, json, base64, re, socket, websockets, asyncio, time, logging, pprint, webbrowser, sys, websocket
from datetime import datetime

#######################
# Customize with your server info, user credential, and Terminal ID from here

url = "https://myistra.<your-domain-com>/restletrouter"
message = "ENDUSER:<user_login>:<user_pass>"
mon_term = "+33xxxxxxxxx"

# Stop customizing, or do it once you know what to do!
#######################

message_bytes = message.encode()
base64_bytes = base64.b64encode(message_bytes)
base64_message = base64_bytes.decode()
type(base64_message)

# Defining the initial header

payload = {
    'X-Application': 'myCrm',
    'Content-Type': 'application/json',
    'Accept': 'application/json, text/plain, */*'}
payload['Authorization'] = "Basic " + base64_message

# Login phase

login = requests.post(url + "/v1/service/Login/", headers=payload)
r_dict = login.headers

# Getting X-Application and Set-Cookie headers, beautifying them (useful later)

xapp = r_dict['X-Application']
cookie = r_dict['Set-Cookie']
cookie = r_dict['Set-Cookie'].split('myCrm_SESSIONID=')[1].split(';')[0]
cookie = "myCrm_SESSIONID=" + cookie

payload_auth = {
    'Content-Type': 'application/json',
    'Accept': 'application/json, text/plain, */*'}
payload_auth['X-Application'] = xapp
payload_auth['Cookie'] = cookie

# Calling the crm/Info method to get some general information on User

crm_info = requests.get(url + "/v1/crm/Info", headers=payload_auth)
crm_data = json.loads(crm_info.text)
istra_version = crm_data['istraVersion']
username = crm_data['userName']
email = crm_data['user']['emails']
extension = crm_data['extension']['addressNumber']
presence = crm_data['extension']['presenceState']
pstn = crm_data['extension']['pstnNumbers']

# Displaying general info
print(f"\n===================================================\nISTRA VERSION:{istra_version}\nUSERNAME:{username}\nEMAIL:{email}\nEXTENSION:{extension}\nPRESENCE:{presence}\nPSTN:{pstn}\n===================================================\n")

# Get User terminals

get_term = requests.get(f"{url}/v1/crm/GetTerminals", headers=payload_auth)
get_term_json = json.loads(get_term.text)

# Identify the terminal to monitor later

for x in range(0,len(get_term_json)):
    if "terminalName" in get_term_json[x]:
        if get_term_json[x]['terminalName'] == mon_term:
            terminalid = get_term_json[x]['terminalId']

if terminalid == "":
    print("We have not found your terminal. Sorry. Exiting")

# Creating the WebSocket to receive live events from the server

url_ws = "wss://myistra.centrex.centile.net/restletrouter"

async def mycrm_socket():

    async with websockets.connect(url_ws + "/ws-service/myCrm", extra_headers=payload_auth) as websocket:
        
        # Put listener on user's terminal calls

        callLine_get = requests.get(f"{url}/v1/crm/CallLine?terminalId={terminalid}&listenerName=CallLines", headers=payload_auth)
        callLine_get_json = json.loads(callLine_get.text)
                
        # Put listener on ACD Groups calls
        
        ACDgroups_get = requests.get(f"{url}/v1/acd/ACDGroupAddress?listenerName=ACDSupervision", headers=payload_auth)
        ACDgroups_get_json = json.loads(ACDgroups_get.text)
        #pprint.pprint(ACDgroups_get_json)

        # Details of ACD Groups

        print("\n===================================================")
        print("ACD Groups List")
        print("===================================================")

        for x in range(0,ACDgroups_get_json['count']):
            print(f"{x+1}: {ACDgroups_get_json['results'][x]['addressNumber']} \
                PSTN(s): {ACDgroups_get_json['results'][x]['pstnNumbers']}")

        # Details of ACD Group members

        ACDmembers_get = requests.get(f"{url}/v1/acd/GroupAddressMember", headers=payload_auth)
        ACDmembers_get_json = json.loads(ACDmembers_get.text)
        #pprint.pprint(ACDmembers_get_json)

        print("\n===================================================")
        print("ACD groups details the supervisor is member of")
        print("===================================================")
        for x in range(0, ACDmembers_get_json['count']):
            print(f"{x+1}: {ACDmembers_get_json['results'][x]['abstractGroupAddress']['label']} - Ext: {ACDmembers_get_json['results'][x]['abstractGroupAddress']['addressNumber']} - Active members: {ACDmembers_get_json['results'][x]['abstractGroupAddress']['activeMembers']} - Nb of members: {len(ACDmembers_get_json['results'][x]['abstractGroupAddress']['members'])} - Queue size: {ACDmembers_get_json['results'][x]['abstractGroupAddress']['queueSize']}")

        if callLine_get_json and "callFlowId" in callLine_get_json[0]:
            if callLine_get_json[0]['incoming'] == False:
                print(f"CallLines: Outgoing {str(callLine_get_json[0]['type'])} call with {str(callLine_get_json[0]['displayNumberE164'])} ({str(callLine_get_json[0]['displayLabel'])})")
            else:
                print(f"CallLines: Incoming {str(callLine_get_json[0]['type'])} call with {str(callLine_get_json[0]['displayNumberE164'])}")

        t1 = datetime.now()
        print("\n===================================================")
        print(f"We are monitoring calls of {mon_term}")
        
        # Keeping the loop for 30s for demo purpose, logout after receiving next message after 30s

        while (datetime.now()-t1).seconds <= 30:
            rcv = await websocket.recv()
            rcv_json = json.loads(rcv)
            #pprint.pprint(rcv_json)
            
            # Information on the websocket

            if "address" in rcv_json:
                print("Web Socket details:")
                print("Address: " + str(rcv_json['address']))
                #print("Max Length: " + str(rcv_json['maxFrameLength']))
                #print("Max Message Size: " + str(rcv_json['maxMessageSize']))
                print("Timeout: " + str(rcv_json['timeout_milliseconds']))
            print("===================================================")

            # Listener: GroupsACDSupervisor

            if ("listenerName" in rcv_json) and rcv_json['listenerName'] == "ACDSupervision":
                
                if rcv_json['item'][-1]['event'] == "callRinging":
                    print(f" ACDSupervision - ACD Call from {rcv_json['item'][0]['displayNumber']} to {rcv_json['item'][0]['lastRedirected']['label']} ({rcv_json['item'][0]['lastRedirected']['addressNumber']}) ringing (CallId: {rcv_json['item'][0]['callId']})")       

                if rcv_json['item'][-1]['event'] == "callTalking":
                    print(f" ACDSupervision - ACD Call from {rcv_json['item'][0]['displayNumber']} to {rcv_json['item'][0]['lastRedirected']['label']} ({rcv_json['item'][0]['lastRedirected']['addressNumber']}) established (CallId: {rcv_json['item'][0]['callId']})")
            
                if rcv_json['item'][-1]['event'] == "callDropped":
                    print(f" ACDSupervision - ACD Call from {rcv_json['item'][0]['displayNumber']} to {rcv_json['item'][0]['lastRedirected']['label']} ({rcv_json['item'][0]['lastRedirected']['addressNumber']}) terminated (CallId: {rcv_json['item'][0]['callId']})")
                    call_s = int(rcv_json['item'][0]['talkingTime'])/1000
                    call_s = int(call_s//1)
                    print(f"    + Duration: " + str(call_s) + "s")

                #pprint.pprint(rcv_json)
            
            # Listener: CallLines

            elif ("listenerName" in rcv_json) and rcv_json['listenerName'] == "CallLines":
                            
                # Call in talking state

                if "item" in rcv_json and str(rcv_json['item'][0]['event']) == "callTalking":
                    print(f" CallLines - Call Established with {rcv_json['item'][0]['displayNumber']} (CallId: {rcv_json['item'][0]['callId']})")
                    
                    if rcv_json['item'][0]['incoming'] == False:
                        print(f" CallLines - Outgoing {rcv_json['item'][0]['type']} call with {rcv_json['item'][0]['displayNumber']} ({rcv_json['item'][0]['displayLabel']}) (CallId: {rcv_json['item'][0]['callId']})")
                    
                    else:
                         print(f" CallLines - Incoming {rcv_json['item'][0]['type']} call with {rcv_json['item'][0]['displayNumberE164']} (CallId: {rcv_json['item'][0]['callId']})")

                # Call in held state

                if "item" in rcv_json and str(rcv_json['item'][0]['event']) == "callHeld":
                    print(f" CallLines - Call Held (CallId: {rcv_json['item'][0]['callId']})")

                # Call in dropped state

                if "item" in rcv_json and rcv_json['item'][0]['event'] == "callDropped":
                    print(f" CallLines - Call Terminated with {rcv_json['item'][0]['displayNumber']} (CallId: {rcv_json['item'][0]['callId']})")
                    call_s = int(rcv_json['item'][0]['talkingTime'])/1000
                    call_s = int(call_s//1)
                    print(f"    + Duration: " + str(call_s) + "s")
            else: 
                print("...")
                
asyncio.get_event_loop().run_until_complete(mycrm_socket())

# Logout phase

logout = requests.get(f"{url}/v1/service/Logout", headers=payload_auth)
logout_json = json.loads(str(logout.status_code))


if "204" in str(logout_json):
    print(f"Logout ok ({logout_json})")
else:
    print(f"Error during Logout ({logout_json})")



